
//------------------------------------------------------------------------------
// Global config ---------------------------------------------------------------

#define DEBUG 0
#define HAND_TYPE LEFT
#define MAIN_LOOP_DELAY 40
#define KEYPRESS_DELAY 500
#define SERIAL_SPEED 9600
#define REPEAT_COUNTER 6
