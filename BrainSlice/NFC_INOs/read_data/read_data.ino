#include <SPI.h>
#include <MFRC522.h>
#include <Keyboard.h>

#define SS_PIN 10
#define RST_PIN 9

MFRC522 mfrc522(SS_PIN, RST_PIN);   
MFRC522::MIFARE_Key key;

void setup() {
    //Serial.begin(115200); 
    //while (!Serial);    
    Keyboard.begin();
    SPI.begin();        
    mfrc522.PCD_Init(); 

    // Подготовим ключ
    // используем ключ FFFFFFFFFFFFh который является стандартом для пустых карт
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    //Serial.println(F("Scan a MIFARE Classic PICC to demonstrate read and write."));
    //Serial.print(F("Using key (for A and B):"));
    //dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
    //Serial.println();
    
    //Serial.println(F("BEWARE: Data will be written to the PICC, in sector #1"));
}

void loop() {
  
  delay(200);
    // Ждем новую карту
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;

    // Выбираем одну из карт
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;

    // Показываем подробности карты
    //Serial.print(F("Card UID:"));
    //dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    //Serial.println();
    //Serial.print(F("PICC type: "));
    byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    //Serial.println(mfrc522.PICC_GetTypeName(piccType));

    // Проверяем совместимость
    if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        Serial.println(F("This sample only works with MIFARE Classic cards."));
        return;
    }

    // В этом примере мы используем первый сектор данных карты, блок 4
    byte sector         = 1;
    
    byte trailerBlock   = 7;
    byte status;
    
    // Аутентификация
    //Serial.println(F("Authenticating using key A..."));
    status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        //Serial.print(F("PCD_Authenticate() failed: "));
        //Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }


    // Аутентификация
    //Serial.println(F("Authenticating again using key B..."));
    status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        //Serial.print(F("PCD_Authenticate() failed: "));
        //Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    byte enc_block[] = {
      };
    

    //Serial.print(F("Reading data from blocks ")); 
    //Serial.println(F(" ..."));

    byte buf[18] = {0}; 
    String result = "";
    
    for (int i = 4, j = 0; i < 7; i++, j += 16) {
      byte size = sizeof(buf);
      status = mfrc522.MIFARE_Read(i, buf, &size);
      if (status != MFRC522::STATUS_OK) {
        //Serial.print(F("Read fail"));
        //Serial.println(mfrc522.GetStatusCodeName(status));
        return;
      }

      //dump_byte_array(buf, 16); 
      char temp_char;     
      for (int k = 0; k < 16; k++) {
        temp_char = enc_block[j * 16 + k] ^ buf[j * 16 + k];
        if (temp_char)
          result += temp_char;
        else
          break;
      }
      
      if (temp_char == 0) break;
           
    }

    // Выводим данные
    //
    //Serial.println(F("Current data in sector:"));
    //mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
    //Serial.println();

    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();


    Keyboard.press(0xB0);
    Keyboard.releaseAll();  
    delay(1000);
     
    Keyboard.print(result);
    Keyboard.releaseAll();

    delay(100);
    
    Keyboard.press(0xB0);
    Keyboard.releaseAll();
    

}

void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        //Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        //Serial.print(buffer[i], HEX);
    }
}
