#include <Wire.h>
#include <MPU6050.h>

#include "key_map.h"
//#include <SparkFun_MPU9960.h>
#include <SoftwareSerial.h>

#define MPU9960_INT    6

//SparkFun_MPU9960 apds = SparkFun_MPU9960();

#define RIGHT 0
#define LEFT 1

//------------------------------------------------------------------------------
// Global config ---------------------------------------------------------------

#include "config.h"

//------------------------------------------------------------------------------

struct KEY_INPUT_STATE{
  const int lowercase_pin = 14;
  const int uppercase_pin = 4;
  const int pin_id[INPUT_KEYS_COUNT] = {9, 8, 7, 6, 5};
  int tdelay[2][INPUT_KEYS_COUNT] = {{0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};
  KEY_POSITION state[INPUT_KEYS_COUNT] = {PRESSED};
};

struct Hand{
  unsigned int position;
  KEY_INPUT_STATE keys;
};

struct MPU_state{
  int X;
  int Y;
};

bool KEY_FLAG_UP = false;
bool PINTEST_ENABLE = false;


//------------------------------------------------------------------------------
// Prototypes ------------------------------------------------------------------

// DEBUGging
inline void DEBUG_notify(const char* message);
void DEBUG_format_keys_state(KEY_INPUT_STATE* key_input);

// MPU sensor processing

void MPU_init();

inline int  MPU_process_interrupt();
inline int  MPU_get_state();
inline void MPU_interrupt_routine();
inline void MPU_convert_direction(int direction, int* row, int* col);

// Keyboard misc routines

inline void KEYS_key_up(char input);
inline void KEYS_key_down(char input);
inline void KEYS_convert_position_to_coords(int position, int* row, int* col);
inline  int KEYS_convert_coords_to_position(int row, int col);

// Sticky keys logic

inline bool is_sticky(char key_symbol);
inline bool is_locked(char key_symbol);
inline void lock_key(char key_symbol);
inline void remove_lock(char key_symbol);

// Lock logic. In case if hand removed from table

inline void LOCK_lock_hand();
inline void LOCK_unlock_hand();
inline bool LOCK_is_hand_locked();
inline bool LOCK_is_hand_removed(int direction);
inline bool LOCK_is_hand_placed_back(int direction);
inline void LOCK_process_input(KEY_INPUT_STATE* key_input);

// Physical GPIO logic

inline void PINS_init();
inline void PINS_get_signal(KEY_INPUT_STATE* key_input);

// GPIO I/O Test

void PINTEST_set_test_pins();
void PINTEST_collect_pins_states();
void PINTEST_run();

// Main hand processing

void HAND_init(Hand* hand);
void HAND_get_state(Hand* hand);
int  HAND_get_position(int current_position, int direction);
void HAND_process_keys(int position, KEY_INPUT_STATE* key_input);

//------------------------------------------------------------------------------
// DEBUGging rotuines

inline void DEBUG_notify(const char* message) {

  //!  test
  /*
  if(DEBUG) {
    Serial.println(message);
  }
  */
  Serial.println(message);
  // OLED here
}

void DEBUG_format_keys_state(KEY_INPUT_STATE* key_input) {
    char raw_data[] = "00000";
    
    for(int i = 0; i < INPUT_KEYS_COUNT; i++) {
      if (key_input->state[i] == PRESSED)
        raw_data[i] = '1';
    }    
    Serial.println(raw_data);
}


//------------------------------------------------------------------------------
// Keyboard misc routines ------------------------------------------------------


inline void KEYS_convert_position_to_coords(int position, int* row, int* col) {
  *row = position / KEY_COLS;
  *col = position % KEY_ROWS;
}

inline int KEYS_convert_coords_to_position(int row, int col) {
  return row * KEY_COLS + col;
}

inline void KEYS_key_down(char input) {
  if (!DEBUG) {
    Keyboard.press(input);
  }
}

inline void KEYS_key_up(char input) {
  if (!DEBUG) {
    Keyboard.release(input);
  }
}

//------------------------------------------------------------------------------
// MPU 6050 Routines. Required to process hand direction ----------------------


MPU6050 mpu;

enum MPU_Direction
{
  DIR_LEFT,
  DIR_RIGHT,
  DIR_UP,
  DIR_DOWN,
  DIR_NONE,
  DIR_FAR,
  DIR_NEAR
};

MPU_Direction prevstate = DIR_NONE;


void MPU_init() {

  Wire.begin();
  mpu.initialize();
  if (!mpu.testConnection()) {
    while (1);
  }

  DEBUG_notify("MPU was initialized");
}


inline void MPU_get_state(MPU_state* newmpu) {
 
  int16_t ax, ay, az, gx, gy, gz;

  mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  newmpu->X = gy / 200;
  newmpu->Y = (gx + 300) / 200;

  /*
  Serial.print(newmpu->X);
  Serial.print(' ');
  Serial.println(newmpu->Y);
  
  char cstr[16];
  itoa(newmpu->X, cstr, 10); 
  DEBUG_notify(cstr);
  
  itoa(newmpu->Y, cstr, 10); 
  DEBUG_notify(cstr);
  */
}

unsigned long prev_event_time =  millis();

int MPU_get_direction() {
  const int gyro_limit = 80;
  const int gyro_delay = 600;

  MPU_state newmpu;
  MPU_get_state(&newmpu);
  int result = DIR_NONE;
  
  if (millis() - prev_event_time < gyro_delay) return DIR_NONE;

  if (newmpu.Y > gyro_limit) {
     DEBUG_notify("UP");
    result = DIR_UP;
  } else if (newmpu.Y < -gyro_limit) {
     DEBUG_notify("DOWN");
    result = DIR_DOWN;
  } else if (newmpu.X > gyro_limit) {
     DEBUG_notify("LEFT");
    result = DIR_LEFT;
  } else if (newmpu.X < -gyro_limit) {
     DEBUG_notify("RIGHT");
    result = DIR_RIGHT; 
  }

  if (result != DIR_NONE) {
    prev_event_time = millis();
  }

  return result;
}

int MPU_get_direction_() {
  const int gyro_limit = 100;

  MPU_state newmpu;
  MPU_get_state(&newmpu);
  if (newmpu.Y > gyro_limit) {
    // UP
    switch (prevstate) {
      case DIR_DOWN: {
        prevstate = DIR_NONE;
        break;
      }
      case DIR_UP: {
        return DIR_NONE;
      }
      default: {
        
        DEBUG_notify("UP");
        prevstate = DIR_UP;
      }
    };
    return prevstate;

  } 

  if (newmpu.Y < -gyro_limit) {
    // DOWN
    switch (prevstate) {
      case DIR_UP: {
        prevstate = DIR_NONE;
        break;
      }
      case DIR_DOWN: {
        return DIR_NONE;
      }
      default: {
        DEBUG_notify("DOWN");
        prevstate = DIR_DOWN;
      }
    }
    return prevstate;
  } 

  if (newmpu.X > gyro_limit) {
    // LEFT
    switch (prevstate) {
      case DIR_RIGHT: {
        prevstate = DIR_NONE;
        break;
      } 
      case DIR_LEFT: {
        return DIR_NONE;
      }
      default: {
        DEBUG_notify("LEFT");
        prevstate = DIR_LEFT;
      }
    }
    return prevstate;
  }

  if (newmpu.X < -gyro_limit)
  {
    // RIGHT
    switch (prevstate) {
      case DIR_LEFT: {
        prevstate = DIR_NONE;
        break;
      }
      case DIR_RIGHT: {
        return DIR_NONE;
      }
      default: {
        DEBUG_notify("RIGHT");
        prevstate = DIR_RIGHT;
      }
    }
    return prevstate;
  }

  return DIR_NONE;
}

inline void MPU_convert_direction(int direction, int* row, int* col) {
  
  switch (direction) {
    case DIR_UP:
      *col += 1;
      break;
    case DIR_DOWN:
      *col -= 1;
      break;
    case DIR_LEFT:
      *row -= 1;
      break;
    case DIR_RIGHT:
      *row += 1;
      break;
  }   

  if(*row == -1) {
    *row = 0;
  }

  if(*row == KEY_ROWS) {
    *row = KEY_ROWS - 1;
  }

  if(*col == -1) {
    *col = 0;
  }

  if(*col == KEY_COLS) {
    *col = KEY_COLS - 1;
  }
}


//------------------------------------------------------------------------------
// Sticky keys routines --------------------------------------------------------

inline bool is_sticky(char key_symbol) {
  for (int i = 0; i < STICKY_KEYS_COUNT; ++i)
  {
    if(key_symbol == sticky_keys[i]) {
      return true;
    }
  }
  return false;
}


inline bool is_locked(char key_symbol) {
  for (int i = 0; i < STICKY_KEYS_COUNT; ++i)
  {
    if(key_symbol == locked_sticky_keys[i]) {
      return true;
    }
  }
  return false;  
}

inline void lock_key(char key_symbol) {
  for (int i = 0; i < STICKY_KEYS_COUNT; ++i)
  {
    if(locked_sticky_keys[i] == 0) {
      locked_sticky_keys[i] = key_symbol;
    }
  }

}

inline void remove_lock(char key_symbol) {
  for (int i = 0; i < STICKY_KEYS_COUNT; ++i)
  {
    if(key_symbol == locked_sticky_keys[i]) {
      locked_sticky_keys[i] = 0;
    }
  }
}

// General hand processing routines --------------------------------------------

Hand hand;

void HAND_init(Hand* hand) {
  if(HAND_TYPE == LEFT) {
    hand->position = 1;
  }
  else {
    hand->position = KEY_COLS - 2;
  }
}

int HAND_get_position(int current_position, int direction) {
  int current_row, current_col;
  KEYS_convert_position_to_coords(current_position, &current_row, &current_col);
  MPU_convert_direction(direction, &current_row, &current_col);
  return KEYS_convert_coords_to_position(current_row, current_col);
}

void HAND_process_keys(int position, KEY_INPUT_STATE* key_input) {
  int row, col;

  KEYS_convert_position_to_coords(position, &row, &col);
  char* current_row = KEY_FLAG_UP ? key_mapping_shift[row][col] : key_mapping[row][col];
  
  for(int i = 0; i < INPUT_KEYS_COUNT; i++) {
    int signal_state = key_input->state[i];
    //DEBUG_notify(signal_state == PRESSED ? "PRESSED" : "RELEASED");
    //Switch between fingers order for left and right hand

    char input_symbol = HAND_TYPE == LEFT ? current_row[(INPUT_KEYS_COUNT - 1) - i] : \
                                             current_row[i];
    //int* tdelay = key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i]

    if (signal_state == PRESSED) {
      //if(key_input->tdelay[i] == 0) {
      //  KEYS_key_down(input_symbol); 
      //}

      if(DEBUG) { Serial.println((unsigned char)input_symbol); }

      //! Test
      DEBUG_notify("Get HIGH pin");
      DEBUG_notify(input_symbol);      
      Serial.println(String(key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i]));

      // tdelay should be calibrated according to the user preferencess
      key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i]++;

      /* Sticky keys are switching between states by full down/up
          Key pressed -> KEYS_key_down + KEYS_key_up
          Key release -> KEYS_key_down + KEYS_key_up
      */

      /*
      if (is_sticky(input_symbol)) {
        if(!is_locked(input_symbol)) {
          KEYS_key_down(input_symbol);
        }
        return;
      }*/

      // If key is NOT "sticky", repeat input of the key
      

      if (key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i] > REPEAT_COUNTER) {
        //! BLOCKED FOR TESTING
        /*
        // KEYS_key_down(input_symbol);
        // delay(KEYPRESS_DELAY);
        // KEYS_key_up(input_symbol);
        */
        DEBUG_notify("Key input");
        delay(500);

        key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i] = 0;
      }
    } else {
      
      /*
      if(is_sticky(input_symbol)) {
        if(!is_locked(input_symbol)) {
          lock_key(input_symbol);
          return;
        } else {
          remove_lock(input_symbol);
        }
      }
      */

      //if (key_input->tdelay[i] > 0) {
      //  KEYS_key_up(input_symbol);
      //}
      //! Test
      if (key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i] > 0) {
        DEBUG_notify("released");
        Serial.println(String(key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i]));
        key_input->tdelay[KEY_FLAG_UP ? 1 : 0][i] = 0;
      }
    }
  }
}


int previous_state = -1;

void HAND_get_state(Hand* hand) {
  
  MPU_Direction direction = MPU_get_direction();
  /*
  if (LOCK_is_hand_removed(direction)) {
    LOCK_lock_hand();
    return;
  }

  if (LOCK_is_hand_placed_back(direction) && LOCK_is_hand_locked()) {
    LOCK_process_input(&hand->keys);
    return;
  }
  */

  if (direction != DIR_NONE) {
    hand->position = HAND_get_position(hand->position, direction);
  }

  if (previous_state != hand->position) {
    char cstr[16];
    itoa(hand->position, cstr, 10); 
    DEBUG_notify(cstr);
    previous_state = hand->position;
  }
}

//------------------------------------------------------------------------------
// Hand lock/unlock routines ---------------------------------------------------

enum HAND_LOCK_STATE
{
  UNLOCKED,
  REMOVED,
  LOCKED,
  PLACED_BACK
};

HAND_LOCK_STATE hand_lock = UNLOCKED;
const unsigned int lock_delay = 5;
const unsigned int lock_pin = 0;
unsigned int press_delay = 0;

inline bool LOCK_is_hand_removed(int direction) {
  return direction == DIR_FAR;
}

inline bool LOCK_is_hand_placed_back(int direction) {
  return direction == DIR_NEAR;
}

inline bool LOCK_is_hand_locked() {
  return hand_lock == LOCKED;
}

inline void LOCK_lock_hand() {
  hand_lock = LOCKED;
};

inline void LOCK_unlock_hand() {
  hand_lock = UNLOCKED; 
}

inline void LOCK_process_input(KEY_INPUT_STATE* key_input) {
  KEY_POSITION signal_state = key_input->state[lock_pin];
  
  if (signal_state == PRESSED) {
    press_delay++;
  }

  if (signal_state == RELEASED) {
    if (press_delay > lock_delay) {
      LOCK_unlock_hand();
    }

    press_delay = 0;
  }
}

// Pins routines ---------------------------------------------------------------

inline void PINS_init() {
  for(int i = 0; i < INPUT_KEYS_COUNT; i++) {
        pinMode(hand.keys.pin_id[i], INPUT);
  }
  pinMode(hand.keys.uppercase_pin, OUTPUT);
  pinMode(hand.keys.lowercase_pin, OUTPUT);
}

inline void PINS_get_signal(KEY_INPUT_STATE* key_input){
  bool need_print_DEBUG = false;
  for (int i = 0; i < INPUT_KEYS_COUNT; i++) {
    if(digitalRead(key_input->pin_id[i]) == ON)
      key_input->state[i] = PRESSED;
    else
      key_input->state[i] = RELEASED;
    
    if (DEBUG && key_input->state[i] == PRESSED)
      need_print_DEBUG = true; 
  }

  if (need_print_DEBUG) {
    DEBUG_format_keys_state(key_input);
  }

}

// Pins test routines ----------------------------------------------------------

struct PIN_TEST_CASE{
  const int output_pins[2] = {4, 14};
  const int input_pins[INPUT_KEYS_COUNT]  = {9, 8, 7, 6, 5};
  const int output_count = 2;
};

PIN_TEST_CASE pin_test;

void PINTEST_set_test_pins() {
  for (int x = 0; x < pin_test.output_count; x++)
    pinMode(pin_test.output_pins[x], OUTPUT);


  for (int x = 0; x < INPUT_KEYS_COUNT; x++)
    pinMode(pin_test.input_pins[x], INPUT); 
}

void PINTEST_collect_pins_states() {
  for (int x = 0; x < INPUT_KEYS_COUNT; x++)
    Serial.print(digitalRead(pin_test.input_pins[x]));
}

void PINTEST_run() {
  for (int x = 0; x < pin_test.output_count; x++) {
    digitalWrite(pin_test.output_pins[x], ON);
    Serial.print("ROW ");
    Serial.println(pin_test.output_pins[x]);
    PINTEST_collect_pins_states();
    Serial.println("");
    digitalWrite(pin_test.output_pins[x], OFF);
    delay(40);
  }
  Serial.println("--------------------------------");
  delay(200);
}


//------------------------------------------------------------------------------
// # Setup ---------------------------------------------------------------------

void switch_output() {
  
  if(DEBUG) {
     Serial.begin(SERIAL_SPEED);
  }
  else {
    //! Removed for test 
    Serial.begin(SERIAL_SPEED);
    //Keyboard.begin();
  }
}

void setup() { 
  if (PINTEST_ENABLE) {
    Serial.begin(SERIAL_SPEED);
    PINTEST_collect_pins_states();
    return;
  }
  
  switch_output();
  
  MPU_init();
  PINS_init();
  HAND_init(&hand);
}


//------------------------------------------------------------------------------
// # Main Loop -----------------------------------------------------------------

void loop(){
  
  if (PINTEST_ENABLE) {
    PINTEST_run();
    return;
  }
  
  // Update position of the hand
  HAND_get_state(&hand);

  /* 
    So, MK4 has two rows of keys on each hand
    I am going to use upper line of keys for uppercase
  */
  
    KEY_FLAG_UP = false;
    digitalWrite(hand.keys.lowercase_pin, ON);
    PINS_get_signal(&hand.keys);
    HAND_process_keys(hand.position, &hand.keys);
    digitalWrite(hand.keys.lowercase_pin, OFF);
  
    KEY_FLAG_UP = true;
    digitalWrite(hand.keys.uppercase_pin, ON);
    PINS_get_signal(&hand.keys);
    HAND_process_keys(hand.position, &hand.keys);
    digitalWrite(hand.keys.uppercase_pin, OFF);
    
  delay(MAIN_LOOP_DELAY);
}


//------------------------------------------------------------------------------
