import os
from serial import *
from aes_impl import AESCipher
import pyautogui as PGUI

aes = AESCipher(os.environ['PRNG'])

com_port = Serial(\
				port=os.sys.argv[1], \
				baudrate=115200, \
				bytesize=EIGHTBITS, \
				parity=PARITY_NONE, \
				stopbits=STOPBITS_ONE, \
				timeout=0.1, \
				xonxoff=0, \
				rtscts=0, \
				interCharTimeout=None \
			)

def perform_auth(encrypted_key):
	print "KEY"
	encrypted_key = encrypted_key[len("[KEY START] "):-len(" [KEY END]\n")].replace(' ', '').decode('hex')
	PGUI.press(aes.decrypt(encrypted_key))
	PGUI.press("enter")

buffer_string = ''
while True:
	buffer_string += com_port.read(com_port.inWaiting())

	if '\n' in buffer_string:		
		perform_auth(buffer_string)
		buffer_string = ''
		continue
		lines = buffer_string.split('\n') # Guaranteed to have at least 2 entries
		buffer_string = lines[-1]
		print buffer_string

	time.sleep(0.05)