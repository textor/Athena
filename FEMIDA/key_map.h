#include <Keyboard.h>

#define ON HIGH
#define OFF LOW

enum KEY_POSITION
{
  RELEASED,
  PRESSED
};

#define KEY_ROWS 4
#define KEY_COLS 4
#define INPUT_KEYS_COUNT 5


const char sticky_keys[] = {
  KEY_CAPS_LOCK,
  KEY_LEFT_CTRL,
  KEY_LEFT_ALT,
  KEY_LEFT_SHIFT,
  KEY_LEFT_GUI
};

const int STICKY_KEYS_COUNT = sizeof(sticky_keys);
char locked_sticky_keys[STICKY_KEYS_COUNT] = {0};

const char key_mapping[KEY_ROWS][KEY_COLS][INPUT_KEYS_COUNT] = {
  {
    {'0', KEY_TAB, KEY_CAPS_LOCK, KEY_LEFT_CTRL, KEY_LEFT_ALT}, 
    {'1', '1', '2', '3', '4'}, 
    {'2', '5', '6', '7', '8'}, 
    {'3', '9', '0', '-', '='}
  },

  {
    {'4', 'q', 'w', 'e', 'r'}, 
    {'5', 't', 'y', 'u', 'i'}, 
    {'6', 'o', 'p', '[', ']'}, 
    {'7', KEY_DELETE, KEY_BACKSPACE, KEY_RIGHT_ALT, KEY_RIGHT_CTRL}
  },

  {
    {'8', 'a', 's', 'd', 'f'}, 
    {'9', 'g', 'h', 'j', 'k'}, 
    {'A', 'l', ';', '\'', KEY_RETURN}, 
    {'B', KEY_HOME, KEY_END, KEY_PAGE_DOWN, KEY_PAGE_UP}
  }, 

  {
    {'C', KEY_LEFT_SHIFT, 'z', 'x', 'c'}, 
    {'D', 'v', 'b', 'n', 'm'}, 
    {'E', ',', '.', '/', KEY_RIGHT_SHIFT}, 
    {'F', KEY_RIGHT_ARROW, KEY_LEFT_ARROW, KEY_DOWN_ARROW, KEY_UP_ARROW}
  }  
};

const char key_mapping_shift[KEY_ROWS][KEY_COLS][INPUT_KEYS_COUNT] = { 
  {
    {'0', KEY_TAB, KEY_CAPS_LOCK, KEY_LEFT_CTRL, KEY_LEFT_ALT}, 
    {'1', '!', '@', '#', '$'}, 
    {'2', '%', '^', '&', '*'}, 
    {'3', '(', ')', '_', '+'}
  },

  {
    {'4', 'Q', 'W', 'E', 'T'}, 
    {'5', 'T', 'Y', 'U', 'I'}, 
    {'6', 'O', 'P', '{', '}'}, 
    {'7', KEY_DELETE, KEY_BACKSPACE, KEY_RIGHT_ALT, KEY_RIGHT_CTRL}
  },

  {
    {'8', 'A', 'S', 'D', 'F'}, 
    {'9', 'G', 'H', 'J', 'K'}, 
    {'A', 'L', ':', '|', KEY_RETURN}, 
    {'B', KEY_HOME, KEY_END, KEY_PAGE_DOWN, KEY_PAGE_UP}
  }, 
  
  {
    {'C', KEY_LEFT_SHIFT, 'Z', 'X', 'C'}, 
    {'D', 'V', 'B', 'N', 'M'}, 
    {'E', '<', '>', '?', KEY_RIGHT_SHIFT}, 
    {'F', KEY_RIGHT_ARROW, KEY_LEFT_ARROW, KEY_DOWN_ARROW, KEY_UP_ARROW}
  }  
};
